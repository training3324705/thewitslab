import React from 'react'
import { StyleTellUs,TopGap, Text, TextSpan, RightImage, RightImageWrapper, SmallText, LeftTextWrapper, ButtonWrapper, ButttonIconWrapper,IconWrapper, SocialMediaWrapper, SocialMediaIcons, SocialMediaText, Line, SocialIconsWrapper } from '../styledComponents/StyleTellUs'
import firstimg from "../assets/Images/tellus-image.png"
import Linkedin from  "../assets/Images/Vector1.png"
import Fb from  "../assets/Images/ExcludeF.png"
import Twitter from  "../assets/Images/ExcludeT.png"
import Youtube from  "../assets/Images/ExcludeY.png"
// import { FaAngleRight } from 'react-icons/fa'

function TellUsSection() {  
  return (  
    <>
        <TopGap/>
    <StyleTellUs>
        <LeftTextWrapper>   
            <Text>We're not just IT. We're a <TextSpan>Business Transformation Company.</TextSpan></Text>
            <SmallText>We are more than just a business. We're a community of like-minded individuals who share our love for connecting people with the best in their area.</SmallText>
            
            <ButttonIconWrapper>
                <ButtonWrapper>Tell Us About your Project</ButtonWrapper>
                <IconWrapper>1</IconWrapper>
                {/* <FaAngleRight/> */}
            </ButttonIconWrapper>

            <SocialMediaWrapper>
                <SocialMediaText>Follow US</SocialMediaText>
                <Line/>
                <SocialIconsWrapper>
                    <SocialMediaIcons src={Linkedin} />
                    <SocialMediaIcons src={Fb} />
                    <SocialMediaIcons src={Twitter} />
                    <SocialMediaIcons src={Youtube} />
                </SocialIconsWrapper>
            </SocialMediaWrapper>

            
        </LeftTextWrapper>

        <RightImageWrapper>
                <RightImage src={firstimg} alt="image"/>
        </RightImageWrapper>
    </StyleTellUs>
    </>
  )
}

export default TellUsSection