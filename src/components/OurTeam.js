import React from 'react'
import {Empty, OurTeamHeading, OurTeamTextImgWrapper,OurTeamTextWrapper,TopTeamText,AnswersText, TeamMembersWrapper, BgWrapper, BgImg, FirstDiv,CenterImg,SecDiv,FirstImg,ThirdDiv, SecImg, FourthDiv, FifthDiv, ThirdImg, SixthDiv, SeventhDiv, EighthDiv, FourImg, NinethDiv, FiveImg, TenthDiv, EleventhDiv,SixImg, TwelthDiv, ThirtheenthDiv, SevenImg, DeveloperText, FounderText, DesignerText, UxText, DesText, MembersTextWrapper, NumText, MemberText, AnimatorText, SrDevWrapper, SrDevName, SrDevPos, GapSide } from '../styledComponents/StyleOurTeam'
import BgImage from "../assets/Images/teambg.png"
import Center from "../assets/Images/center.png"
import Ist from "../assets/Images/ist.png"
import Sec from "../assets/Images/2nd.png"
import Third from "../assets/Images/3rd.png"
import FourImgg from "../assets/Images/4thImg.png"
import FifthImg from "../assets/Images/5Img.png"
import SixImgg from "../assets/Images/6thImg.png"
import SevenImgg from "../assets/Images/7thImg.png"


function OurTeam() {
  return (
  
    <OurTeamTextImgWrapper>
      <OurTeamTextWrapper>
        <TopTeamText>Our Expert</TopTeamText>
        <OurTeamHeading>
        Meet Our Team of Experts
          <Empty />
        </OurTeamHeading>
        <AnswersText>
        You have questions. We have answers.
        </AnswersText>
      </OurTeamTextWrapper>

      <TeamMembersWrapper>
        <GapSide/>
            <BgWrapper>
              <BgImg src={BgImage}/>
              <FirstDiv>
                <CenterImg src={Center}/>
              </FirstDiv>

              <SrDevWrapper>
                <SrDevName>Julia Carter</SrDevName>
                <SrDevPos>Sr. Developer</SrDevPos>
              </SrDevWrapper>

              <SecDiv>
                <FirstImg src={Ist}/>
              </SecDiv>

              <ThirdDiv>
                <SecImg src={Sec}/>
              </ThirdDiv>

              <FourthDiv/>
              <DeveloperText>Developer</DeveloperText>
              <FifthDiv>
                <ThirdImg src={Third}/>
              </FifthDiv>

              <SixthDiv/>
              <FounderText>2 Founders</FounderText>

              <SeventhDiv/>
              <DesignerText>
                <UxText>UX</UxText>
                <DesText>Designers</DesText>
              </DesignerText>

              <EighthDiv>
                  <FourImg src={FourImgg}/>
              </EighthDiv>

              <NinethDiv>
                <FiveImg src={FifthImg}/>
              </NinethDiv>

              <TenthDiv/>
              <MembersTextWrapper>
                <NumText>200+</NumText>
                <MemberText>Members</MemberText>
              </MembersTextWrapper>

              <EleventhDiv>
                <SixImg src={SixImgg}/>
              </EleventhDiv>

              <TwelthDiv/>
              <AnimatorText>Animator</AnimatorText>

              <ThirtheenthDiv>
                <SevenImg src={SevenImgg}/>
              </ThirtheenthDiv>

            </BgWrapper>
      </TeamMembersWrapper>
      </OurTeamTextImgWrapper>  
  )
}

export default OurTeam