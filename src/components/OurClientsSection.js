import React from "react";
import {
  OurClientsTextImgWrapper,
  OurClientsTextWrapper,
  OurClientsImgWrapper,
  TopClientsText,
  OurClientsText,
  WinningBussinessText,
  Empty,
  WinningBussinessTextTwo,
  OurClientsImg,
  Div,
} from "../styledComponents/StyleOurClients";

import Spotify from  "../assets/Images/Spotify.png";
import Google from  "../assets/Images/Google.png";
import Amazon from  "../assets/Images/Amazon.png";
import Microsoft from  "../assets/Images/microsoft.png";
import Netflix from  "../assets/Images/netflix.png";

function OurClientsSection() {
  return (
    <>
    <Div/>
      <OurClientsTextImgWrapper>
      <Div/>
      <OurClientsTextWrapper>
        <TopClientsText>Top Clients</TopClientsText>
        <OurClientsText>
          Our Clients
          <Empty />
        </OurClientsText>
        <WinningBussinessText>
          These powerhouses are already winning business with us.
        </WinningBussinessText>
        <WinningBussinessTextTwo>
          We love what we do, and we hope you'll let us help you too.
        </WinningBussinessTextTwo>
      </OurClientsTextWrapper>

      <Div/>
      <OurClientsImgWrapper>
        <OurClientsImg src={Spotify}/>
        <OurClientsImg src={Google}/>
        <OurClientsImg src={Amazon}/>
        <OurClientsImg src={Microsoft}/>
        <OurClientsImg src={Netflix}/>
      </OurClientsImgWrapper>
    </OurClientsTextImgWrapper>
    </>
  );
}

export default OurClientsSection;
