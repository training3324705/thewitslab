import React from "react";
import {
  Empty,
  SolutionsAllText,
  SolutionsHeading,
  SolutionsText,
  SolutionsWrapper,
  QuestionsAnswersWrapper,
  QuestionsWrapper,
  Questions,
  QuestionsSep,
  TotalQuestions,
  AnswerWrapper,
  HadQuestion,
  SolutionsHeadingWrapper
} from "../styledComponents/StyleSolutions";
// import Question from '../QuestionContent.json'

function SolutionsSection() {
  return (
    <SolutionsWrapper>
     
      <SolutionsAllText>
        <SolutionsText>Solutions</SolutionsText>

        <SolutionsHeadingWrapper>
          <SolutionsHeading>Problem Statements</SolutionsHeading>
          <Empty />
        </SolutionsHeadingWrapper>
      </SolutionsAllText>

        <QuestionsAnswersWrapper>
            <QuestionsWrapper>
                <Questions>
                    What are you looking for?
                </Questions>
                <QuestionsSep/>

              
                <TotalQuestions>
                    Question 1 of 2
                </TotalQuestions>
            </QuestionsWrapper> 
            <AnswerWrapper>
            <HadQuestion>I want a website or app that will change how we do business</HadQuestion>
            <HadQuestion>I need to Transform an idea to a product</HadQuestion>
            <HadQuestion>I want to give the best experience to my visitors to increase conversions</HadQuestion>
            <HadQuestion>I want to leverage latest tech</HadQuestion>
            <HadQuestion>I want to add a bit of pep and improve my operations</HadQuestion>
            <HadQuestion>I am looking to save some bucks</HadQuestion>
            <HadQuestion>I am just here to explore</HadQuestion>
           
                {/* {Question.map((item, index)=>{
                    return( 
                        <>
                            <HadQuestion>{item.q1}</HadQuestion>
                            <HadQuestion>{item.q2}</HadQuestion>
                            <HadQuestion>{item.q3}</HadQuestion>
                            <HadQuestion>{item.q4}</HadQuestion>
                            <HadQuestion>{item.q5}</HadQuestion>
                            <HadQuestion>{item.q6}</HadQuestion>
                            <HadQuestion>{item.q7}</HadQuestion>
                        </>
                    )
                })} */}
            </AnswerWrapper>
        </QuestionsAnswersWrapper>
    </SolutionsWrapper>
    
  );
}

export default SolutionsSection;
