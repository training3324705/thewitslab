import React from "react";
import {
  OurNums,
  OurNumStrong,
  OurNumsWrapper,
  OurNumsText,
  OurNumsTextHeading,
  OurNum,
  Empty,
  Empty2,
  InNumsWrapper,
  OurNumsTextWrapper,
  LinesOfCodeCount,
  LinesOfCodeText,
  TeamMembersCount,
  TeamMembersText,
  CompletedProjectsCount,
  CompletedProjectsText,
  CodeWrapper,
  TeamWrapper,
  ProjectWrapper,
  Empty3,
  RestText,
  FirstGap,
} from "../styledComponents/StyleOurNums";

function OurNumbersSection() {
  return (
    
    <OurNumsWrapper>
      <FirstGap/>
      <OurNums>
        <OurNum>Achievement</OurNum>
            <OurNumStrong>Our Numbers<Empty /></OurNumStrong>   
      </OurNums>

      <OurNumsTextWrapper>
        <OurNumsText>
          <OurNumsTextHeading>When you see our numbers, you'll know why we're the best.</OurNumsTextHeading> 
         <RestText> We're a fastest growing community working hard to make things happen. We've got
          a lot of hands-on cumulative experience and a good amount of fire under
          our tails, though we are young.</RestText>
          <Empty2/> 
        </OurNumsText>
        <InNumsWrapper>
            <CodeWrapper> 
                <LinesOfCodeCount>500K+</LinesOfCodeCount>
                <LinesOfCodeText>Lines of Code</LinesOfCodeText>
            </CodeWrapper>
            <Empty3/>
            <TeamWrapper>
                <TeamMembersCount>140K+</TeamMembersCount>
                <TeamMembersText>Team Members</TeamMembersText>
            </TeamWrapper>
            <Empty3/>
            <ProjectWrapper>
                <CompletedProjectsCount>65K+</CompletedProjectsCount>
                <CompletedProjectsText>Completed Projects</CompletedProjectsText>
            </ProjectWrapper>
        </InNumsWrapper>
      </OurNumsTextWrapper>

      
    </OurNumsWrapper>
  );
}

export default OurNumbersSection;
