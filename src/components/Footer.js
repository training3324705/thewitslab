import React from "react";
import {
  FooterContainer,
  Gap,
  Logo,
  LogoWrapper,
  TextWrapper,
  TopWrapper,
  Text,
  SocialMediaWrapper,
  SocialText,
  SocialIcons,
  MiddleWrapper,
  AboutUsWrapper,
  GeneralWrapper,
  ContactUsWrapper,
  SubscribeWrapper,
  SocialInput,
  SocialInputWrapper,
  SubscribeButton,
  AboutUsLink,
  GeneralLink,
  ContactUsLink,
  SocialText1,
  LineTop,
  LineBetween,
  LastWrapper,
  TermsConWrapper,
  PrivacyWrapper,
  LegalWrapper,
  CopywWrapper,
  LineBetween2,
  LineBetween3,
  //   LineBetween1
  //   LineSoc
} from "../styledComponents/StyleFooter";
import LogoImg from "../assets/Images/logo.png";
import {
  FaLinkedin,
  FaFacebook,
  FaTwitter,
  FaYoutube,
  FaEnvelope,
} from "react-icons/fa";
function Footer() {
  return (
    <>
      <Gap />
      <FooterContainer>
        <TopWrapper>
          <LogoWrapper>
            <Logo src={LogoImg} />
          </LogoWrapper>
          <TextWrapper>
            <Text>
              zzril Ut quis feugait nibh ea aliquip erat ut minim velit vel qui
              sed dolore illum nulla consequat. tincidunt nisl iriure feugiat
              Duis eros laoreet vulputate dolore nostrud eu tation odio
              facilisi. dignissim consectetuer volutpat.
            </Text>
          </TextWrapper>
          <LineBetween3 />

          <SocialMediaWrapper>
            <SocialText1>Follow Us</SocialText1>
            <SocialIcons>
              <FaLinkedin className="Sicons" />
              <FaFacebook className="Sicons" />
              <FaTwitter className="Sicons" />
              <FaYoutube className="Sicons" />
            </SocialIcons>
            {/* <LineSoc/> */}
          </SocialMediaWrapper>
        </TopWrapper>
        <LineTop />

        <MiddleWrapper>
          <AboutUsWrapper>
            <AboutUsLink>About Us</AboutUsLink>
            <AboutUsLink>Services</AboutUsLink>
            <AboutUsLink>Case Studies</AboutUsLink>
            <AboutUsLink>Our Process</AboutUsLink>
            <AboutUsLink>Industries</AboutUsLink>
          </AboutUsWrapper>
          <LineBetween />
          <GeneralWrapper>
            <GeneralLink>General</GeneralLink>
            <GeneralLink>Company</GeneralLink>
            <GeneralLink>Career</GeneralLink>
            <GeneralLink>Blogs</GeneralLink>
            <GeneralLink>Contact Us</GeneralLink>
          </GeneralWrapper>
          <LineBetween2 />
          <ContactUsWrapper>
            <ContactUsLink>Contact Us</ContactUsLink>
            <ContactUsLink>SCO 40-41 D,</ContactUsLink>
            <ContactUsLink>
              3rd floor, City Heart, Kharar, Punjab-140301
            </ContactUsLink>
            <ContactUsLink>Unit 37 Tileyard Road, N7 9AH</ContactUsLink>
            <ContactUsLink>California, United States</ContactUsLink>
          </ContactUsWrapper>

          <SubscribeWrapper>
            <SocialText>Subscribe to Our Newsletter</SocialText>
            <SocialInputWrapper>
              <FaEnvelope className="emailIcon" />
              <SocialInput placeholder="Enter Your Email" />
              <SubscribeButton>Subscribe</SubscribeButton>
            </SocialInputWrapper>
          </SubscribeWrapper>
        </MiddleWrapper>
        <LastWrapper>
          <TermsConWrapper>Terms & Conditions</TermsConWrapper>
          <PrivacyWrapper>Privacy Policy</PrivacyWrapper>
          <LegalWrapper>Legal</LegalWrapper>
          <CopywWrapper>
            Copyright © 2020. witsinnovationlab. All rights reserved.
          </CopywWrapper>
        </LastWrapper>
      </FooterContainer>
    </>
  );
}

export default Footer;
