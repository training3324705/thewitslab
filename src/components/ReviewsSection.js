import React from "react";
import {

  Empty,
  ReviewsButtonWrapper,
  ReviewsTextButtonWrapper,
  ReviewsFrtext,
  ReviewsFtext,
  ReviewsFvtext,
  ReviewsMainWrapper,
  ReviewsNextButton,
  ReviewsNextButtonImg,
  ReviewsPreButton,
  ReviewsPreButtonImg,
  ReviewsSliderWrapper,
  ReviewsStext,
  ReviewsTextWrapper,
  ReviewsTtext,
  ReviewsSlider,
  ReviewImgWrapper,
  ReviewImg,
  ReviewNameWrapper,
  ReviewName,
  ReviewEmp,
  ReviewRating,
  ReviewRatingImg,
  ReviewText,
} from "../styledComponents/StyleReviews";
import Pre from "../assets/Images/Pre.png";
import Next from "../assets/Images/Next.png";
import Review from "../assets/Images/Review.png";
import Rating from "../assets/Images/Rating.png";

function ReviewsSection() {
  return (
    
      <ReviewsMainWrapper>
        <ReviewsTextButtonWrapper>
          <ReviewsTextWrapper>
            <ReviewsFtext>Reviews</ReviewsFtext>
            <ReviewsStext>
              Testimonials
              <Empty />
            </ReviewsStext>
            <ReviewsTtext>
              We are the best, and our customers say it.
            </ReviewsTtext>
            <ReviewsFrtext>
              We've never been ones to brag, but we can't help but share some of
              the feedback
            </ReviewsFrtext>
            <ReviewsFvtext>
              we've gotten from our clients. Take a look at what they have to
              say about us:
            </ReviewsFvtext>
          </ReviewsTextWrapper>

          <ReviewsButtonWrapper>
            <ReviewsPreButton>
              <ReviewsPreButtonImg src={Pre} />
            </ReviewsPreButton>
            <ReviewsNextButton>
              <ReviewsNextButtonImg src={Next} />
            </ReviewsNextButton>
          </ReviewsButtonWrapper>
        </ReviewsTextButtonWrapper>

      
          <ReviewsSliderWrapper>
            <ReviewsSlider>
              <ReviewImgWrapper>
                <ReviewImg src={Review} />
              </ReviewImgWrapper>
              <ReviewNameWrapper>
                <ReviewName>Jane Cooper</ReviewName>
                <ReviewEmp>Manager at Google</ReviewEmp>
                <ReviewRating>
                  <ReviewRatingImg src={Rating} />
                </ReviewRating>
              </ReviewNameWrapper>
            </ReviewsSlider>
             <ReviewText>
                "Thanks guys, keep up the good work! I wish I would have thought of
                it first. It's incredible. I don't always clop, but when I do, it's
                because of Wits Innovation Lab."
              </ReviewText>
          </ReviewsSliderWrapper>
          
        
      </ReviewsMainWrapper>
  );
}

export default ReviewsSection;
