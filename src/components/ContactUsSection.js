import React from "react";
import {
  Empty,
  ContactAllText,
  ContactHeading,
  ContactText,
  HadProjectContactWrapper,
  HadProjectWrapper,
  Questions,
  ContactWrapper,
  Gap,
  ContactUsWrapper,
  Gap2,
  InterestedIn,
  InputAllSer,
  InputName,
  InputEmail,
  InputTellUs,
  InterestedText,
  InputWrapper,
  AddAttachWrapper,
  AttachLink,
  AttachText,
  ButtonWrapper,
  Button,
  SepWrapper,
  Line,
  Or,
  GetInWrapper,
  GetInText,
  SocialMedWrapper,
  WhatAppWrapper,
  CalWrapper,
  WhatAppButton,
  CalButton,
} from "../styledComponents/StyleContactUs";
import { FaLink } from 'react-icons/fa'
import { FaWhatsapp } from 'react-icons/fa'
import { FaCalendar } from 'react-icons/fa'


function ContactUs() {
  return (
    <ContactUsWrapper>
      <Gap/>
      <ContactAllText>
        <ContactText>Our Expert</ContactText>
        <ContactHeading>
        Contact Us
          <Empty />
        </ContactHeading>
        <ContactText>You have questions. We have answers.</ContactText>
      </ContactAllText>

        <Gap2/>
        <HadProjectContactWrapper>
            <HadProjectWrapper>
                <Questions>
                Have a Project?
                We would love to help
                </Questions>
            </HadProjectWrapper> 
            <ContactWrapper>
                <InterestedIn>
                    <InterestedText>I’m interested in....</InterestedText>
                </InterestedIn>
                <InputWrapper>
                    <InputAllSer placeholder="All Services" />
                </InputWrapper>
                <InputWrapper>
                <InputName placeholder="Your Name"/>
                </InputWrapper>
                <InputWrapper>
                <InputEmail placeholder="Your Email"/>
                </InputWrapper>
                <InputWrapper>
                    <InputTellUs placeholder="Tell us about your project"/>
                </InputWrapper>
                <AddAttachWrapper>
                    <AttachLink>
                        <FaLink/>
                    </AttachLink>
                    <AttachText>
                        Add Attachment
                    </AttachText>
                </AddAttachWrapper>
                <ButtonWrapper>
                    <Button>Send Request</Button>
                </ButtonWrapper>
                <SepWrapper>
                    <Line/>
                    <Or>or</Or>
                    <Line/>
                </SepWrapper>
                <GetInWrapper>
                    <GetInText>
                        Get in touch through
                    </GetInText>
                </GetInWrapper>
                <SocialMedWrapper>
                    <WhatAppWrapper>
                        <WhatAppButton><FaWhatsapp/>Whatsapp</WhatAppButton>
                    </WhatAppWrapper>
                    <CalWrapper>
                        <CalButton><FaCalendar/>Calendly</CalButton>
                    </CalWrapper>
                </SocialMedWrapper>
            </ContactWrapper>
        </HadProjectContactWrapper>
    </ContactUsWrapper>
    
  );
}

export default ContactUs;
