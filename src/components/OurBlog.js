import React from "react";
import {
  BlogAllText,
  BlogHeading,
  BlogText,
  BlogWrapper,
  Empty,
  Gap,
  LearnText,
  Text2,FirstImgTextWrapper,Img1,Img2,Img3, SecondImg, SecondImgWrapper, TextImgWrapper, ThirdImg, Text,AllTextBlog,BlogChainText,CommercialText,Gap1, ImgWrapper} from "../styledComponents/StyleOurBlog";
import Blog1 from "../assets/Images/Blog1.png"
import Blog2 from "../assets/Images/Blog2.png"
import Blog3 from "../assets/Images/Blog3.png"



function OurBlog() {
  return (
    <>
      <Gap />
    <BlogWrapper>
      <BlogAllText>
        <LearnText>Learn & Grow</LearnText>
        <BlogHeading>
          Our Blog
        </BlogHeading>
          <Empty />
        <BlogText>
          We're here to make your thoughts on tech a little more… thought-ful.
        </BlogText>
      </BlogAllText>
      <Gap1/>
      <TextImgWrapper>
          <FirstImgTextWrapper>
            <ImgWrapper>
              <Img1 src={Blog1} />
            </ImgWrapper>
            <AllTextBlog>
              <Text>WIL Blog</Text>
              <Text2>Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat</Text2>
            </AllTextBlog>
          </FirstImgTextWrapper>
          <SecondImgWrapper>
            <SecondImg><Img2 src={Blog2} /></SecondImg>
            <BlogChainText>Block Chain</BlogChainText>
            <ThirdImg><Img3 src={Blog3} /></ThirdImg>
            <CommercialText>Commercial</CommercialText>
          </SecondImgWrapper>
      </TextImgWrapper>
    </BlogWrapper>
    </>
  );
  
}

export default OurBlog;
