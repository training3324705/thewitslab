import React from "react";
import {
  LinksSpan,
  LinksWrapper,
  Logo,
  NavButtonWrapper,
  NavButton,
  SpanImgWrapper,
  StyledNavbar,
  LogoWrapper,
} from "../styledComponents/NavBar";
import pic from "../assets/Images/logo.png";
import { FaAngleDown } from 'react-icons/fa'

function Navbar() {
  return (
    <StyledNavbar>
      <LogoWrapper>
        <Logo src={pic} alt="logo" />
      </LogoWrapper>

      <LinksWrapper>
        <SpanImgWrapper>
          <LinksSpan>About Us <FaAngleDown/></LinksSpan>
        </SpanImgWrapper>

        <SpanImgWrapper>
          <LinksSpan>Industry</LinksSpan>
        </SpanImgWrapper>

        <SpanImgWrapper>
          <LinksSpan>Services<FaAngleDown/></LinksSpan>
        </SpanImgWrapper>

        <SpanImgWrapper>
          <LinksSpan>Career</LinksSpan>
        </SpanImgWrapper>
      </LinksWrapper>

      <NavButtonWrapper>
          <NavButton>Let's Connect</NavButton>
      </NavButtonWrapper>
    </StyledNavbar>
  );
}

export default Navbar;
