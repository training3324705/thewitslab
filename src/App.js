import ContactUs from './components/ContactUsSection';
import Footer from './components/Footer';
import NavBar from './components/NavBar'
import OurBlog from './components/OurBlog';
import OurClientsSection from './components/OurClientsSection';
import OurNumbersSection from './components/OurNumbersSection';
import OurTeam from './components/OurTeam';
import ReviewsSection from './components/ReviewsSection';
import SolutionsSection from './components/SolutionsSection';
import TellUsSection from './components/TellUsSection';
import {MainContainter} from "./styledComponents/MainContainer"


function App() {
  return (
    <MainContainter>
        <NavBar/>
        <TellUsSection/>
         <OurNumbersSection/>  
        <SolutionsSection/> 
        <OurClientsSection/>
        <ReviewsSection/>
        <OurBlog/>
        <OurTeam/>
        <ContactUs/>
        <Footer/>
    </MainContainter>
  );
}

export default App;