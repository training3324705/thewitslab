
import styled from "styled-components";

export const StyledNavbar = styled.div`
  width: 100vw;
  height: 8.333vw;
  display: flex;
  align-items: center;
  justify-content: space-around;  
  font-family: 'inter';
  font-style: normal;
  font-size: 1.111vw;
  padding-top: 0.694vw;
  /* position: fixed;
  width: 100%; */
`;

export const LogoWrapper = styled.div`
`;

export const Logo = styled.img`
  
`;

export const LinksWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-wrap: wrap;
  font-family: 'Inter';
  font-style: normal;
  font-weight: 600;
  font-size: 1.111vw;
`;

export const SpanImgWrapper = styled.div`
&:hover {
        border-bottom: 2px solid #F9C51C;
        cursor: pointer;
  }
`
export const LinksSpan = styled.span`
    padding: 1vw;
`;

export const NavButtonWrapper = styled.div`

`

export const NavButton = styled.button`
  background-color: #f9c51c;
  padding: 0.833vw 1.25vw;
  border: none;

  &:hover {
    border: 1px solid black;
    cursor: pointer;
  }
`;
