import styled from 'styled-components'


export const BlogWrapper = styled.div`
    margin-left: 9.722vw;
`

export const Gap = styled.div`
    width: 100vw;
    height: 20vh;`

export const Gap1 = styled.div`
width: 100vw;
height: 8.889vh;`

export const BlogAllText = styled.div``

export const LearnText = styled.div`
    color: #828282;
      font-size: 1.556vw;`

export const BlogHeading = styled.div`
    font-size: 4.444vw;
    
`

export const Empty = styled.div`
  position: absolute;
  border-top: 1vw solid #f9c51c;
    width: 16.667vw;
    height: 0.694vw;
  z-index: -1;
  transform: translate(0.139vw, -1.8vw);
`

export const BlogText = styled.div``


export const TextImgWrapper = styled.div`
/* border: 2px solid red; */
    width: 100vw;
    height: 45vw;
    display: flex;
    align-items: flex-start;
    gap: 1vw;
`
export const ImgWrapper = styled.div`
        /* width: 44.444vw;
        height: 61.806vw; */
`

export const FirstImgTextWrapper = styled.div`
    /* width: 44.444vw; */
    height: 60vw;
    
`

export const Img1 = styled.img`
width: 35.903vw;
height: 30vw;
`

export const Img2 = styled.img`
width: 28vw;
height: 15.6vw;
`

export const Img3 = styled.img`
width: 28vw;
height: 15.6vw;
`

export const AllTextBlog = styled.div`
background-color: #1D2E88;
padding: 0.8vw;
width: 34.3vw;
height: 9.2vh;   
border-radius: 0.5vw;
color: white;
transform: translate(0,-60px);
`

export const Text = styled.div`
    font-size: 1.667vw;
`
export const Text2 = styled.div`
width: 27vw;
font-size: 1vw;
`

export const SecondImgWrapper = styled.div`

`

export const SecondImg = styled.div`
    
`
export const BlogChainText = styled.div`
        transform: translate(40px,-60px);
        color: white;
        font-weight: 500;
`
export const CommercialText = styled.div`
    transform: translate(40px,-60px);
    color: white;
    font-weight: 500;
`

export const ThirdImg = styled.div`
    /* width: 11.111vw; */
    /* height: 33.403vh;  */
`

