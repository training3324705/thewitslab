import styled from "styled-components";

export const StyleTellUs = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const TopGap = styled.div`
width: 100vw;
height: 5vw;
`
export const LeftTextWrapper = styled.div`
  width: 40vw;
  margin-left: 9vw;
  font-family: "inter";
  height: 40vw;
`;

export const Text = styled.div`
  color: black;
  width: 38.333vw;
  height: 21.389;
  font-size: 4.444vw;
  font-weight: 500;
`;

export const TextSpan = styled.span`
  color: #f9c51c;
  font-weight: 500;
`;

export const SmallText = styled.div`
  width: 38.333vw;
  height: 7vw;
  font-size: 1.25vw;
  font-weight: 100;
`;

export const ButttonIconWrapper = styled.div``;

export const ButtonWrapper = styled.button`
  width: 15vw;
  height: 4vh;
  background-color: #1d2e88;
  color: #fff;
  padding: 1.111vw 1.389vw 2vw 1.111vw;
  border: none;
  cursor: pointer;
  font-size: 1.042vw;
`;

export const IconWrapper = styled.button`
  width: 3vw;
  height: 4vh;
  background-color: #f9c51c;
  padding: 1.111vw 1.389vw 2vw 1.111vw;
  border: none;
  cursor: pointer;
  font-size: 1.042vw;
`;

export const SocialMediaWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  height: 15vh;
`;

export const SocialMediaText = styled.div`
  width: 5.278vw;
  height: 1.667vw;
  font-size: 1.111vw;
`;
export const Line = styled.div`
  width: 5.278vw;
  border-bottom: 2px solid #1d1d1d;
  margin: 0 1.319vw;
`;
export const SocialIconsWrapper = styled.div``;

export const SocialMediaIcons = styled.img`
  width: 1.6vw;
  height: 1.6vw;
  padding: 0 0.8vw;

  /* &:hover{
       background-color: #F9C51C;
    } */
`;

export const RightImageWrapper = styled.div`
  width: 48vw;
  height: 48vw;
  position: absolute;
  display: flex;
  justify-content: flex-end;
  box-sizing: border-box;
  top: 20px;
  right: 0px;
  z-index: -1;
  opacity: 0.8;
`;

export const RightImage = styled.img`
  width: 47vw;
  height: 47vw;
`;
