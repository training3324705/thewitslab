import styled from "styled-components";
import bgImage from  "../assets/Images/dot-image.png"

export const MainContainter = styled.div`
  /* width: 100%; */
  /* max-width: 1440px; */
  width: 100vw;
  margin: 0 auto;
  background-image: url(${bgImage});
`;