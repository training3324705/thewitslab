import styled from "styled-components";

export const ReviewsMainWrapper = styled.div`
     
`

export const ReviewsTextButtonWrapper= styled.div`
        display: flex;
        align-items: center;
        justify-content: center;
        width: 100vw;
    height: 30vw;
`

export const ReviewsTextWrapper = styled.div`
font-family: 'Inter';
font-style: normal;
font-weight: 500;
font-size: 1.389vw;
`

export const ReviewsFtext = styled.div`
    color: #828282;
    font-size: 1.389vw;

`
export const ReviewsStext = styled.div`
        color: #1D1D1D;
        font-size: 3.75vw; 
`
export const Empty = styled.div`
position: absolute;
  border-top: 1vw solid #f9c51c;
    width: 19.444vw;
    height: 0.694vw;
  z-index: -1;
  transform: translate(0.139vw, -1.5vw);
`;
export const ReviewsTtext = styled.div`
        color: #4F4F4F;
        font-size: 1.389vw;
        height: 3.472vw;

`
export const ReviewsFrtext = styled.div`
color: #4F4F4F;
        font-size: 1.111vw;

`
export const ReviewsFvtext = styled.div`
    color: #4F4F4F;
    font-size: 1.111vw;
`

export const ReviewsButtonWrapper = styled.div`
        width: 13.889vw;
        height: 10.417vw;
        display: flex;
        justify-content: flex-end;
       align-items: flex-end;
`

export const ReviewsPreButton = styled.div`
      
`
export const ReviewsPreButtonImg = styled.img`
width: 4.306vw;
height: 4.306vw;
      
`
export const ReviewsNextButton = styled.div`
        margin-left: 0.694vw;
        
`

export const ReviewsNextButtonImg = styled.img`
    width: 4.306vw;
height: 4.306vw;
`
export const ReviewWrapper = styled.div`

`

export const ReviewsSliderWrapper = styled.div`
    margin-left: 25.556vw;
    background-color: #F1F1F1;
    width: 41.25vw;
    height: 28.75vw;
    padding: 0.347vw;
    border: 1px solid grey;
    border-radius: 0.347vw;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    color: #1D1D1D;
`
export const ReviewsSlider = styled.div`
    display: flex;   
    align-items: center; 
    justify-content: center;
    gap: 2vw;
`

export const ReviewImgWrapper = styled.div`
`

export const ReviewImg = styled.img`
width: 10.694vw;
height: 10.694vw;
`

export const ReviewNameWrapper = styled.span`
`

export const ReviewName = styled.div`
font-family: 'Inter';
font-style: normal;
font-weight: 700;
font-size: 1.667vw;
color: #1D1D1D;
`
export const ReviewEmp = styled.div`    
font-family: 'Inter';
font-style: normal;
font-weight: 400;
font-size: 1.111vw;
color: #94A3B8;
`
export const ReviewRating = styled.div`
`

export const ReviewRatingImg = styled.img`
width: 10.417vw;
/* height: 10.417vw; */
`

export const ReviewText = styled.div`
/* border: 2px solid red; */
    width: 24vw;
    height: 5vw;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 1.1vw;
`
