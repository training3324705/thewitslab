import styled from "styled-components"

export const OurTeamTextImgWrapper = styled.div`
    width: 91.5vw;
`  

export const GapSide = styled.div`
    width: 8vw;
    height: 50vw;
`
export const OurTeamTextWrapper = styled.div`
     display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    height: 20vw;
`
export const TopTeamText = styled.div`
    color: #828282;
    font-size: 20px;
`

export const OurTeamHeading = styled.div`
color: #1D1D1D;
  font-size: 3.75vw;
  z-index: 0;
`

export const Empty= styled.div`
position: absolute;
  border-top: 1vw solid #f9c51c;
    width: 41vw;
    height: 1vw;
  z-index: -1;
  transform: translate(0.139vw, -1.5vw);
`


export const AnswersText = styled.div`
    color: #4F4F4F;
    font-size: 20px;
`

export const TeamMembersWrapper = styled.div`
    /* border: 2px solid yellow; */
    height: 88vh;
    display: flex;
    align-items: center;
    justify-content: center;
    `

export const BgWrapper = styled.div`
        position: relative;
        /* margin-left: 100px; */
`
export const BgImg = styled.img`
    width:61.017vw;
    height: 57.314vw;
    
`

export const FirstDiv = styled.div`
    width: 23vw;
    height: 23vw;
    position: absolute;
    right:19vw;
    top:15vw;
`

export const SrDevWrapper = styled.div`
    position: absolute;
    right:25vw;
    top:30vw;
    color: white;
`
export const SrDevName = styled.div`
font-weight: 900;
font-size: 1.667vw;
`
export const SrDevPos = styled.div`
    font-size: 1.111vw;
`

export const CenterImg = styled.img`
     width: 23vw;
    height: 23vw;
`
export const SecDiv = styled.div`
    width: 12.563vw;
    height: 12.563vw;
    position: absolute;
    right:49vw;
    top:10vw;
`
export const FirstImg = styled.img`
    width: 12.563vw;
    height: 12.563vw;
`

export const ThirdDiv = styled.div`
    /* border: 2px solid red;
    border-radius: 50%;
    background-color: blue; */
    width: 11.694vw;
    height: 11.694vw;
    position: absolute;
    right: 44vw;
    top: 25vw;
`
export const SecImg = styled.img`
    width: 11.694vw;
    height: 11.694vw;
`

export const FourthDiv = styled.div`
    /* border: 2px solid red; */
    border-radius: 50%;
    width: 8.313vw;
    height: 8.25vw;
    background-color: #1D2E88;
    position: absolute;
    right: 56vw;
    top: 23vw;
`
export const DeveloperText = styled.div`
 position: absolute;
    right: 57vw;
    top: 26vw;
    color: white;
    font-size: 1.389vw;
    `

export const FifthDiv = styled.div`
    /* border: 2px solid red;
    border-radius: 50%;
    background-color: #1D2E88; */
    width: 10.232vw;
    height: 10.232vw;
    position: absolute;
    right: 36vw;
    top: 36vw;
`

export const ThirdImg = styled.img`
    width: 10.232vw;
    height: 10.232vw;
`

export const SixthDiv = styled.div`
    /* border: 2px solid red; */
    border-radius: 50%;
    width: 7.813vw;
    height: 7.813vw;
    background-color: #F9C51C;
    position: absolute;
    right: 51vw;
    top: 38vw;
`
export const FounderText = styled.div`
    position: absolute;
    right: 52vw;
    top: 41vw;
    font-weight: 600;
    font-size: 1.111vw;
    `

export const SeventhDiv = styled.div`
/* border: 2px solid red; */
    border-radius: 50%;
    width: 8.125vw;
    height: 8.188vw;
    background-color: #1D2E88;
    position: absolute;
    right: 23vw;
    top: 41vw;
`
export const DesignerText = styled.div`
    position: absolute;
    right: 25vw;
    top: 44vw;
    color: white;
    `
export const UxText = styled.div`
    margin-left: 1vw;
    font-weight: 900;
    font-size: 1.5vw;
`
export const DesText = styled.div`
font-size: 1vw;
        
`

export const EighthDiv = styled.div`
    /* border: 2px solid red;
    border-radius: 50%;
    background-color: #1D2E88; */
    width: 11.153vw;
    height: 11.153vw;
    position: absolute;
    right: 7vw;
    top: 33vw;
`

export const FourImg = styled.img`
    width: 11.153vw;
    height: 11.153vw;
`

export const NinethDiv = styled.div`
    /* border: 2px solid red;
    border-radius: 50%;
    background-color: #1D2E88; */
    width: 11.423vw;
    height: 11.423vw;
    position: absolute;
    right: -3vw;
    top: 20vw;
`
export const FiveImg = styled.img`
    width: 11.423vw;
    height: 11.423vw;
`

export const TenthDiv = styled.div`
    /* border: 2px solid red; */
    border-radius: 50%;
    width: 6.989vw;
    height: 6.989vw;
    background-color: #F9C51C;
    position: absolute;
    right: 7vw;
    top: 14vw;
`

export const MembersTextWrapper = styled.div`
    position: absolute;
    right: 8vw;
    top: 16vw;
`

export const NumText = styled.div`
font-weight: 900;
font-size: 1.806vw`

export const MemberText = styled.div`
font-size:1vw;
`

export const EleventhDiv = styled.div`
    /* border: 2px solid red; */
    border-radius: 50%;
    width: 9.5vw;
    height: 9.5vw;
    background-color: #F9C51C;
    position: absolute;
    right: 13vw;
    top: 4vw;
`

export const SixImg = styled.img`
    width: 9.5vw;
    height: 9.5vw;
`

export const TwelthDiv = styled.div`
    /* border: 2px solid red; */
    border-radius: 50%;
    width: 8.313vw;
    height: 8.25vw;
    background-color: #1D2E88;
    position: absolute;
    right: 27vw;
    top: 4vw;
`

export const AnimatorText = styled.div`
 position: absolute;
    right: 29vw;
    top: 7vw;
    color: white;
    font-weight: 400;
    font-size: 1.389vw;
`

export const ThirtheenthDiv = styled.div`
    width: 6.768vw;
    height: 6.768vw;
    position: absolute;
    right: 39vw;
    top: 6vw;
`

export const SevenImg = styled.img`
    width: 6.768vw;
    height: 6.768vw;
`