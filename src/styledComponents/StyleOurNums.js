import styled from "styled-components";

export const OurNumsWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  height: 40vw;
`;

export const  FirstGap = styled.div`
  width: 16.667vw;
`

export const OurNums = styled.div`
  transform: rotate(-90deg);
  width: 27.778vw;
`;

export const OurNum = styled.div`
  font-size: 1.389vw;
  color: #828282;
`;

export const OurNumStrong = styled.div`
  font-size: 3.222vw;
  font-weight: 900;
  z-index: 0;
  width: 19.375vw;
  height: 2.5vh;
  z-index: 0;
  
`;

export const Empty = styled.div`
position: absolute;
  border-top: 0.9vw solid #f9c51c;
    width: 20vw;
    height: 1vw;
  z-index: -1;
  transform: translate(0.139vw, -1.5vw);
`;
export const OurNumsTextWrapper = styled.div`
  width: 77.778vw;
`;
export const OurNumsText = styled.div`
  /* border: 2px solid yellow; */
  font-family: "Inter";
  font-style: normal;
  font-size: 1.111vw;
`;

export const RestText = styled.div`
  width: 54.444vw;
  font-size: 1.344vw;
  font-weight: 500;
`
export const OurNumsTextHeading = styled.div`
  font-weight: 700;
  font-size: 1.667vw;
  height: 3.333vh;
`;
export const Empty2 = styled.div`
  background-color: #1d2e88;
  width: 49vw;
  margin-top: 3.472vw;
  height: 5px;
`;

export const InNumsWrapper = styled.div`
  display: flex;
  margin-top: 2.083vw;
`;

export const CodeWrapper = styled.div`
  color: #1d2e88;
  padding-left: 1.111vw;
  padding-right: 4.861vw;
`;

export const LinesOfCodeCount = styled.div`
  font-size: 3.472vw;
  font-weight: 700;
`;
export const LinesOfCodeText = styled.div`
  font-size: 1.111vw;
  font-weight: bolder;
`;

export const TeamWrapper = styled.div`
  color: #1d2e88;
  padding: 0 4.861vw;
`;

export const TeamMembersCount = styled.div`
  font-size: 3.472vw;
  font-weight: 700;
`;
export const TeamMembersText = styled.div`
  font-size: 1.111vw;
  font-weight: bolder;
`;

export const ProjectWrapper = styled.div`
  color: #1d2e88;
  padding: 0 50px;
`;

export const CompletedProjectsCount = styled.div`
  font-size: 3.472vw;
  font-weight: 700;
`;
export const CompletedProjectsText = styled.div`
  font-size: 1.111vw;
  font-weight: bold;
`;

export const Empty3 = styled.div`
  background-color: #1d2e88;
  width: 1px;
  height: 5.167vw;
  margin-top: 0.6vw;
`;

export const Gap = styled.div`
  width: 100%;
  height: 5vh;
`