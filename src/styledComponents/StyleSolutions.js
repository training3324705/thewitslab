import styled from "styled-components";

export const SolutionsWrapper = styled.div`
  /* margin-top: 200px; */
  margin-left: 9vw;
`;
export const Div = styled.div`
    width: 100vw;
    height: 23vh;
`

export const SolutionsAllText = styled.div`
height:10vw`;
export const SolutionsText = styled.div`
      color: #828282;
      font-size: 1.389vw;` 

export const SolutionsHeadingWrapper = styled.div`
     
`;

export const SolutionsHeading = styled.div`
   font-size: 4.444vw;
   
`;

export const Empty = styled.div`
position: absolute;
  border-top: 1vw solid #f9c51c;
    width: 36.111vw;
    height: 0.694vw;
  z-index: -1;
  transform: translate(0.139vw, -1.5vw);
`;

export const QuestionsAnswersWrapper = styled.div`
    display: flex;
    width: 79.861vw;
    border: 2px solid #1D2E88;
    border-radius: 1%;
    height: 55vh;
    font-family: "inter";
`   

export const QuestionsWrapper = styled.div`
  background-color: #1d2e88;
  width: 38.889vw;
  color: #ffffff;
`;

export const Questions = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1.389vw;
`;

export const QuestionsSep = styled.div`
  width:  27.847vw;
  margin-left: 3.472vw;
  height: 2px;
  background-color: #f9c51c;
`;

export const TotalQuestions = styled.div`
  display: flex;
  justify-content: center;
  margin-left: 3.472vw;
`

export const AnswerWrapper = styled.div`
    background-color: #FFFFFF;
    width: 43.333vw;
    display: flex;
    flex-direction: column;
    align-items: baseline;
    height: 55vh;
`
export const HadQuestion = styled.div`
    background-color: #F1F1F1;
    border-radius: 0.694vw;
    margin: 0.556vw;
    padding: 0.694vw;

    &:hover{
      background-color: #1D2E88;
      color: white;
    }
`


