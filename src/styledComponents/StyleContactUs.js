import styled from "styled-components";

export const ContactUsWrapper = styled.div`
  margin-left: 9.722vw;
`;
export const Gap = styled.div`
    width: 100vw;
    height: 8.333vw;
`
export const Gap2 = styled(Gap)`
    height: 5vw;
`

export const ContactAllText = styled.div``;
export const ContactText = styled.div`
      color: #828282;
      font-size: 1.389vw;`

export const ContactHeading = styled.div`
  font-size: 3.75vw;
`;

export const Empty = styled.div`
position: absolute;
  border-top: 1vw solid #f9c51c;
    width: 17.361vw;
    height: 0.694vw;
  z-index: -1;
  transform: translate(0.139vw, -1.5vw);
`;

export const HadProjectContactWrapper = styled.div`
    display: flex;
    width: 85vw;
    border: 1px solid #1D2E88;
    border-radius: 5px;
    height: 55.556vh;
    font-family: "inter";
`   

export const HadProjectWrapper = styled.div`
  background-color: #1d2e88;
  width: 36.667vw;
  color: #ffffff;
   display: flex;
  justify-content: center;
  align-items: center;
  padding: 1.389px;
`;

export const Questions = styled.div`
    width: 25.556vw;
    font-family: 'Inter';
    font-weight: 700;
    font-size: 2.778vw;
`;



export const ContactWrapper = styled.div`
    background-color: #FFFFFF;
    width: 48.333vw;
    display: flex;
    justify-content: space-evenly;
    flex-direction: column;
    margin-left: 2.431vw;
`

export const InterestedIn = styled.div`
`
export const InterestedText = styled.div`
`
export const InputWrapper = styled.div`
`

export const InputAllSer = styled.input`
    width: 33.889vw;
    padding: 0.694vw 1.111vw;
::placeholder{
    color: #828282;
}
`

export const InputName = styled.input`
  outline: 0;
  border-width: 0 0 1px;
  border-color: black;
  /* width: 100%; */
  width: 33.889vw;
  padding: 0.694vw 1.111vw;
::placeholder{
    color: #828282;
}
`
export const InputEmail = styled.input`
 outline: 0;
  border-width: 0 0 1px;
  border-color: black;
  
  width: 33.889vw;
  padding: 0.694vw 1.111vw;
::placeholder{
    color: #828282;
}
`

export const InputTellUs = styled.input`
 outline: 0;
  border-width: 0 0 1px;
  border-color: black;

  width: 33.889vw;
  padding: 0.694vw 1.111vw;
::placeholder{
    color: #828282;
}
`

export const AddAttachWrapper = styled.div`
display: flex;
`
export const AttachLink = styled.div`
`
export const AttachText = styled.div`
`
export const ButtonWrapper = styled.div`
     width: 33vw;
     height: 5vh;
`
export const Button = styled.button`
background-color: #F9C51C;
width: 36.667vw;
height: 5.556vh;

border: none;


&:hover{
    border: 1px solid blue;
}
`

export const SepWrapper = styled.div`
    display: flex;
    align-items: baseline;
    justify-content: baseline;
    margin-left: 4.861vw;
`
export const Line = styled.div`
        width: 10.417vw;
        height: 2px;
        background-color: #333333;
`
export const Or = styled.div`
    padding: 0.694vw;
`

export const GetInWrapper = styled.div`
display: flex;
align-items: center;
justify-content: center;

`
export const GetInText = styled.div`

`
export const SocialMedWrapper = styled.div`

display: flex;
align-items: center;
justify-content: flex-start;
gap: 2.083vw;
`
export const WhatAppWrapper = styled.div`
   
`

export const WhatAppButton = styled.button`
 background-color: #4DCB5B;
 padding: 1.111vw 4.444vw;
 width: 16.736vw;
 border: none;
 &:hover{
    border: 1px solid red;
 }
`
export const CalWrapper = styled.div`
`

export const CalButton = styled.button`
background-color: #006CFE;
padding: 1.111vw 4.444vw;
 width: 16.736vw;
 border: none;

 &:hover{
    border: 1px solid red;
 }
`

