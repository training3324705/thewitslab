
import styled from "styled-components";

export const OurClientsTextImgWrapper = styled.div`
    max-width: 100vw;
    height: 38.542vw;
    background-color: #FFFFFF;
    border-bottom: 0.810417px solid #1D2E88;
    border-top: 0.810417px solid #1D2E88;
`
export const Div = styled.div`  
    width: 35vw;
    height: 7.5vw;
`
export const OurClientsTextWrapper = styled.div`
     display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`
export const TopClientsText = styled.div`
    color: #828282;
    font-size: 1.389vw;
`

export const OurClientsText = styled.div`
    color: #1D1D1D;
  font-size: 3.75vw;
  z-index: 0;
`

export const Empty = styled.div`
 position: absolute;
  border-top: 1vw solid #f9c51c;
    width: 17.361vw;
    height: 0.694vw;
  z-index: -1;
  transform: translate(0.139vw, -1.5vw);
`


export const WinningBussinessText = styled.div`
    color: #4F4F4F;
    font-size: 1.389vw;
    margin-top: 1.389vw;
`
export const WinningBussinessTextTwo = styled.div`
    color: #4F4F4F;
    font-size: 1.111vw;
    margin-top: 1.389vw;
`
export const OurClientsImgWrapper = styled.div`
        display: flex;
        align-items: center;
        justify-content: center;
        gap: 2.778vw;
        flex-wrap: wrap; 
`
export const OurClientsImg = styled.img`
    width: 11.875vw;
    height: 3.611vw ;
`

