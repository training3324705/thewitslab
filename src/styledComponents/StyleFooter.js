import styled from "styled-components";

export const FooterContainer = styled.div`
  background-color: black;
  border-radius: 0 5px 0 0;
  width: 100vw;
  height: 35vw;
  border-top-right-radius: 11.111vw;
`;

export const Gap = styled.div`
  width: 100vw;
  height: 8.333vw;
`;

export const TopWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 2vw;
`;
export const LogoWrapper = styled.div`
  width: 16vw;
  height: 6.111vw;
`;
export const Logo = styled.img`
  width: 10vw;
  height: 5vw;
`;
export const TextWrapper = styled.div`
  width: 35vw;
  height: 6.111vw;
`;

export const Text = styled.div`
  font-family: "Inter";
  font-style: normal;
  font-weight: 400;
  font-size: 1vw;
  color: #ffffff;
  font-size: 1.111vw;
`;

export const SocialMediaWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  color: white;
  width: 29vw;
  height: 6.111vw;
`;
export const SocialText1 = styled.div`
  font-size: 2vw;
`;

export const SocialIcons = styled.div`
  .Sicons {
    /* padding: 2.222vw 1.111vw 1.111vw 0; */
    width: 1.5vw;
    height: 7vw;
    padding-right: 3px;
  }
`;
export const LineTop = styled.div`
  position: absolute;
  border-top: 0.069vw solid white;
  width: 50.694vw;
  transform: translate(10vw, 0);
`;
export const LineBetween = styled.div`
  position: absolute;
  border-left: 0.069vw solid white;
  width: 61vw;
  height: 15vw;
  transform: translate(1vw, 1px);
`;
export const LineBetween2 = styled.div`
  position: absolute;
  border-left: 0.069vw solid white;
  width: 30vw;
  height: 15vw;
  transform: translate(1vw, 1px);
`;

export const LineBetween3 = styled.div`
  position: absolute;
  border-right: 0.069vw solid white;
  width: 29vw;
  height: 24vw;
  transform: translate(0, 9vw);
`;

export const MiddleWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  width: 100vw;
  height: 17vw;
`;

export const AboutUsWrapper = styled.div`
  width: 15vw;
  height: 10vw;
`;
export const AboutUsLink = styled.div`
  padding-bottom: 1.111vw;
  font-size: 1.111vw;
`;

export const GeneralWrapper = styled.div`
  width: 15vw;
  height: 10vw;
`;
export const GeneralLink = styled.div`
  padding-bottom: 1.389vw;
  font-size: 1.111vw;
`;
export const ContactUsWrapper = styled.div`
  width: 32vw;
  height: 10vw;
`;
export const ContactUsLink = styled.div`
  padding-bottom: 1.111vw;
  font-size: 1.111vw;
`;

export const SubscribeWrapper = styled.div`
  width: 17vw;
  height: 10vw;
`;
export const SocialText = styled.div`
  width: 18.333vw;
  height: 8vh;
  font-family: "Inter";
  font-style: normal;
  font-weight: 100;
  font-size: 2vw;
`;
export const SocialInputWrapper = styled.div`
  background-color: #ffffff;
  margin-top: 1.2vw;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 18vw;
  height: 3vw;
  color: black;

  .emailIcon {
    width: 2.222vw;
  }
`;
export const SocialInput = styled.input`
  border: none;
  width: 8.889vw;
  padding: 0.5vw;
  font-size: 1vw;
`;
export const SubscribeButton = styled.button`
  padding: 5px;
  width: 5vw;
  font-size: 1vw;
  background-color: #f9c51c;
  border: none;
`;

export const LastWrapper = styled.div`
  background-color: white;
  margin-top: 1vw;
  width: 100vw;
  height: 3.194vw;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
`;

export const TermsConWrapper = styled.div`
  width: 9.708vw;
  height: 1.25vw;
  font-size: 0.833vw;
`;
export const PrivacyWrapper = styled.div`
  width: 9.708vw;
  height: 1.25vw;
  font-size: 0.833vw;
`;
export const LegalWrapper = styled.div`
  width: 9.708vw;
  height: 1.25vw;
  font-size: 0.833vw;
`;
export const CopywWrapper = styled.div`
  width: 20.708vw;
  height: 1.25vw;
  font-size: 0.833vw;
`;
